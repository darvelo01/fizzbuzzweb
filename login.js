//visit next page with mouse click on button
document.getElementById("submit-button").onclick = function() {
    //grab username from input box and remove white space around it.
    const user = document.querySelector("input").value.trim();
    if (user.length > 0) {
        sessionStorage.setItem('username', user);
        location.href = "home.html";
    }
};

//visit next page by pressing 'enter' key
document.getElementById("user-input").onkeyup = function(event) {
    //grab username from input box and remove white space around it.
    const user = document.querySelector("input").value.trim();
    //look for 'event' key being pressed
    if (event.which === 13 && user.length > 0) {
        sessionStorage.setItem('username', user);
        location.href = "home.html";
    }
};
