//get username from storage and display on screen
const user = sessionStorage.getItem("username");
document.getElementById("user-welcome-msg").innerHTML += user;

//url to user through api
const url = 'http://basic-web.dev.avc.web.usf.edu/' + user;

//Display Score
get(url).then(function(response) {
    //API succesfully finds user's score
    if(response.status == 200){
        //show user's score from last session
        let fbScore = response.data.score;
        showFB(fbScore);

        //update score when pressing button
        document.getElementById("increment-button").onclick = function() {
            const dataToSend = { score: ++fbScore };
            post(url, dataToSend);
            //display updated score
            showFB(fbScore);
        };
    }
    else if (response.status == 404) {
        //User not found. Create one.
        console.log("Creating new user...")
        post(url, { score: 0 }).then(function(response2) {
            let fbScore = response2.data.score;
            showFB(fbScore);

            //update score when pressing button
            document.getElementById("increment-button").onclick = function() {
                const dataToSend = { score: ++fbScore };
                post(url, dataToSend);
                //display updated score
                showFB(fbScore);
            };
        });
    }
});

//get function
function get(url) {
    return new Promise((resolve, reject) => {
        const http = new XMLHttpRequest();
        http.onload = function() {
            resolve({ status: http.status, data: JSON.parse(http.response) });
        };
        http.open("GET", url);
        http.send();
    });
}

//post API function
function post(url, data) {
    data = JSON.stringify(data);
    return new Promise((resolve, reject) => {
        const http = new XMLHttpRequest();
        http.onload = function() {
            resolve({ status: http.status, data: JSON.parse(http.response) });
        };
        http.open("POST", url);
        //Make sure that the server knows we're sending it json data.
        http.setRequestHeader("Content-Type", "application/json");
        http.send(data);
    });
}

//Displays corect FizzBuzz output
function showFB(score) {
    const fizzbuzz = document.getElementById("display-fizzbuzz");

    if (score == 0)
        fizzbuzz.innerHTML = score.toString();
    else if (score%3 == 0 && score%5 == 0)
        fizzbuzz.innerHTML = "FizzBuzz";
    else if (score%3 == 0)
        fizzbuzz.innerHTML = "Fizz";
    else if (score%5 == 0)
        fizzbuzz.innerHTML = "Buzz";
    else
        fizzbuzz.innerHTML = score.toString();
}
